import React from 'react';
import Form from '../components/Form';
import Header from '../components/Header';
import style from  '../styles/NewBook.module.css';

const NewBook = () => (
    <div className={style.main}>
        <Header title="Insira os novos dados"/>
        <div className={style.container}>
            <Form edit={false} />
        </div>
    </div>
)

export default NewBook;
