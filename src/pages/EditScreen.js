import React, { useContext } from 'react';
import Form from '../components/Form';
import Header from '../components/Header';
import BooksContext from '../context/BooksContext';
import style from '../styles/EditScreen.module.css';

const EditScreen = () => {
  const { bookToEdit } = useContext(BooksContext);

  return (
    <div className={style.main}>
      <Header title="Edite os dados abaixo"/>
      <div className={style.container}>
        <Form book={bookToEdit} edit />
      </div>
    </div>
  );
}

export default EditScreen;
