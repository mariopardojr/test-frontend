import React, { useContext } from 'react';
import Header from '../components/Header';
import Table from '../components/Table';
import Loading from '../components/Loading'
import BooksContext from '../context/BooksContext';
import { Link } from 'react-router-dom';
import style from '../styles/Table.module.css'

const Home = () => {
  const { isLoading } = useContext(BooksContext);

  return (
    <>
      <Header title="Akna Library"/>
      <div className={style.container}>
        <Link className={style.addLink} to="/new">Adicionar novo livro</Link>
        {isLoading ? <Loading /> : <Table />}
      </div>
    </>
  );
}

export default Home;
