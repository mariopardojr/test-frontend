import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types'
import getBooks from '../services/getBooks';
import BooksContext from './BooksContext';

const BooksProvider = ({children}) => {
  const [books, setBooks] = useState([]);
  const [bookToEdit, setBookToEdit] = useState({}); 
  const [isLoading, setIsLoading] = useState(false);
  
  useEffect(() => {
    const fecthBooksData = async () => {
      setIsLoading(true);
      const data = await getBooks();
      setBooks(data);
      setIsLoading(false);
    }
    fecthBooksData();
  }, []);

  const context = { books, setBooks, isLoading, bookToEdit, setBookToEdit };

  return (
    <BooksContext.Provider value={ context }>
      {children}
    </BooksContext.Provider>
  );
}

BooksProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export default BooksProvider;
