import React from 'react';
import Home from './pages/Home';
import { Route, Switch } from 'react-router-dom';
import EditScreen from './pages/EditScreen';
import NewBook from './pages/NewBook';

const Routes = () => (
  <Switch>
    <Route path="/edit/:id" component={EditScreen}/>
    <Route path="/new" component={NewBook}/>
    <Route exact path="/" component={Home}/>
  </Switch>
)

export default Routes;
