const URL = 'http://localhost:3000/books';

const postBooks = (data) => {
  fetch(URL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  }).then((response) => response.json())
    .then((data) => {
      console.log('Success', data)
    })
    .catch((error) => {
      console.log('Error', error)
    })
}

export default postBooks;
