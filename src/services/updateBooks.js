const updateBooks = (id, title, author, price) => {
  fetch(`http://localhost:3000/books/${id}`, {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      title,
      author,
      price,
    }),
  }).then((response) => response.json())
    .then((data) => {
      console.log('Success', data)
    })
    .catch((error) => {
      console.log('Error', error)
    })
}

export default updateBooks;
