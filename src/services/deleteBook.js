const deleteBook = (id) => {
  fetch(`http://localhost:3000/books/${id}`, {
    method: 'DELETE',
  }).then((response) => response.json())
    .then((response) => {
      console.log('Success', response)
    })
    .catch((error) => {
      console.log('Error', error)
    })
}

export default deleteBook;
