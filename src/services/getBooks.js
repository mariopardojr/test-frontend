const URL = 'http://localhost:3000/books?_sort=title&_order';

const getBooks = async () => {
  const booksData =  await fetch(URL)
    .then((response) => response.json())
    .catch((error) => error);
  return booksData;
}

export default getBooks;
