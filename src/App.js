import React from 'react';
import BooksProvider from './context/BooksProvider';
import Routes from './routes';

function App() {
  return (
    <BooksProvider>
      <Routes />
    </BooksProvider>
  )
}

export default App;
