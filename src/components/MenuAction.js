import React from 'react';
import PropTypes from 'prop-types';
import style from '../styles/MenuAction.module.css';

const MenuAction = ({children}) => {
  return (
    <nav className={style.navbar}>
      <ul className={style.navbarNav}>
        {children}
      </ul>
    </nav>
  );
}

MenuAction.propTypes = {
  children: PropTypes.node.isRequired,
}


export default MenuAction;
