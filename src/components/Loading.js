import React from 'react';
import style from '../styles/Loading.module.css';

const Loading = () => (
  <div className={style.ldsHourglass} />
)

export default Loading;
