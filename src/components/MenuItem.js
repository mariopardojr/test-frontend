import React, { useState } from 'react';
import PropTypes from 'prop-types';
import style from '../styles/MenuItem.module.css'

const MenuItem = ({ children, icon }) => {
  const [open, setOpen] = useState(false);

  const handleClick = () => {
    setOpen(!open);
  }

  return (
    <li className={style.item}>
      <button className={style.buttonAction} onClick={handleClick}>
        {icon}
      </button>
      {open && children}
    </li>
  );
}

MenuItem.propTypes = {
  icon: PropTypes.node.isRequired,
  children: PropTypes.node.isRequired,
}

export default MenuItem;
