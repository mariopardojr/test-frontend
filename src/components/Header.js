import React from 'react';
import PropTypes from 'prop-types';
import logo from '../images/logo.png';
import style from '../styles/Header.module.css';


const Header = ({title}) => (
  <header>
    <div className={style.container}>
      <img className={style.logo} src={logo} alt="Akna Library Logo" />
      <h2 className={style.title}>{title}</h2>
    </div>
  </header>
)

Header.propTypes = {
  title: PropTypes.string.isRequired,
}

export default Header;
