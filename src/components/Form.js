import React, { useState } from 'react';
import { useHistory } from 'react-router';
import PropTypes from 'prop-types';
import updateBooks from '../services/updateBooks';
import style from '../styles/Form.module.css';
import postBooks from '../services/postBooks';

const Form = ({book, edit}) => {
  const [inputTitle, setInputTitle] = useState(book.title);
  const [inputAuthor, setInputAuthor] = useState(book.author);
  const [inputPrice, setInputPrice] = useState(book.price);
  const history = useHistory();

  const handleChange = ({target}) => {
    if (target.name === 'inputTitle') setInputTitle(target.value);
    else if (target.name === 'inputAuthor') setInputAuthor(target.value);
    else  setInputPrice(target.value);
  }

  const handleClick = () => {
    if (edit) updateBooks(book.id, inputTitle, inputAuthor, inputPrice);
    else postBooks({
      title: inputTitle,
      author: inputAuthor,
      price: inputPrice,
    });
    history.push('/');
  }
  
  return (
    <form>
      <div className={style.inputField}>
        <label htmlFor="inputTitle">
          Título
          <input
            type="text"
            id="inputTitle"
            name="inputTitle"
            value={inputTitle}
            onChange={handleChange}
          />
        </label>
      </div>
      <div className={style.inputField}>
        <label htmlFor="inputAuthor">
          Autor
          <input
            type="text"
            id="inputAuthor"
            name="inputAuthor"
            value={inputAuthor}
            onChange={handleChange}
          />
        </label>
      </div>
      <div className={style.inputField}>
        Preço
        <label htmlFor="inputPrice">
          <input
            type="number"
            id="inputPrice"
            name="inputPrice"
            min="0"
            value={inputPrice}
            onChange={handleChange}
          />
        </label>
      </div>
      <button type="button" onClick={handleClick}>{edit ? 'Atulizar dados' : 'Adicionar'}</button>
    </form>
  );
}

Form.propTypes = {
  book: PropTypes.objectOf(PropTypes.any),
  edit: PropTypes.bool.isRequired,
};

Form.defaultProps = {
  book: {
    title: '',
    author: '',
    price: 0,
  },
};

export default Form;
