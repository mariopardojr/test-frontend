import React, { useContext, useEffect } from 'react';
import BooksContext from '../context/BooksContext';
import { FiMenu, FiEdit, FiTrash2 } from 'react-icons/fi';
import style from '../styles/Table.module.css';
import { useHistory } from 'react-router-dom';
import MenuAction from './MenuAction';
import MenuItem from './MenuItem';
import deleteBook from '../services/deleteBook';
import getBooks from '../services/getBooks';

const Table = () => {
  const headers = ['Título', 'Autor', 'Preço', 'Opções'];
  const { books, setBooks, setBookToEdit } = useContext(BooksContext); 
  const history = useHistory();

  useEffect(() => {
    const fetchData = async () => {
      const data = await getBooks();
      setBooks(data);
    }
    fetchData();
  }, []);

  const handleEdit = (id) => {
    const findBook = books.find((book) => book.id === id)
    setBookToEdit(findBook);
    history.push(`/edit/${id}`)
  }
  const handleDelete = (id) => {
    const newBookList = books.filter((book) => book.id !== id)
    setBooks(newBookList);
    deleteBook(id);
  }

  if (books.length === 0) return (
    <div className={style.empty}>
      <h2>Estoque vazio!</h2>
    </div>
  );

  return (
    <table>
      <thead>
        {headers.map((header) => <th key={ header }>{header}</th>)}
      </thead>
      <tbody>
        {books.map((book) => (
          <tr key={ book.id }>
            <td datalabel="Título">{book.title}</td>
            <td datalabel="Autor">{book.author}</td>
            <td datalabel="Preço">{book.price}</td>
            <td datalabel="Opções">
              <MenuAction>
                <MenuItem icon={<FiMenu />}>
                  <button className={`${style.buttonAction} ${style.green}`} onClick={() => handleEdit(book.id)}><FiEdit /> Editar</button>
                  <button className={`${style.buttonAction} ${style.red}`} onClick={() => handleDelete(book.id)}><FiTrash2 /> Excluir</button>
                </MenuItem>
              </MenuAction>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default Table;
